package main

import (
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	// Route
	"simle_api/routes/test"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Fails to load .env file. Err: %s", err)
	}

	r := gin.Default()

	// CORS
	r.Use(cors.Default())

	// HEALTH CHECK
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	// ASSIGN ROUTE
	test.TestRoute(r)

	// RUN SERVER
	r.Run(":4050") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
