package m_resp

type Response struct {
	Status_Code   int         `json:"-"`
	Message       string      `json:"message,omitempty"`
	Data          interface{} `json:"data,omitempty"`
	Error_Key     string      `json:"error_key,omitempty"`
	Error_Message string      `json:"error_message,omitempty"`
	Error_Data    interface{} `json:"error_data,omitempty"`
}

func SuccessResponse(data interface{}) Response {

	r := Response{
		Status_Code: 200,
		Message:     `Success`,
		Data:        data,
	}

	return r
}

func ErrorIDResponse(em string, id int) Response {

	ed := struct {
		ID int
	}{
		ID: id,
	}

	r := Response{
		Status_Code:   200,
		Message:       `Failed`,
		Error_Key:     "ID_ERROR",
		Error_Message: em,
		Error_Data:    &ed,
	}

	return r
}

func ErrorParamResponse(em string, we_got interface{}) Response {

	r := Response{
		Status_Code:   200,
		Message:       `Failed`,
		Error_Key:     "PARAM_ERROR",
		Error_Message: em,
		Error_Data:    we_got,
	}

	return r
}

func ErrorInternalResponse(em string, clue string) Response {

	ed := struct {
		ON string
	}{
		ON: clue,
	}

	r := Response{
		Status_Code:   200,
		Message:       `Failed`,
		Error_Key:     "INTERNAL_ERROR",
		Error_Message: em,
		Error_Data:    &ed,
	}

	return r
}
