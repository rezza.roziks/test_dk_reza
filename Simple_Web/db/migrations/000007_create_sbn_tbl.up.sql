CREATE TABLE IF NOT EXISTS "SBN" (
	"SBN_ID" integer GENERATED ALWAYS AS identity, --// PK
	"Name" text not null,
	"Ammount" integer not null,
	"Tenor" integer not null,
	"Rate" integer not null,
	"Type" text not null,

    --// CONSTRAINT
    PRIMARY KEY("SBN_ID")
);
