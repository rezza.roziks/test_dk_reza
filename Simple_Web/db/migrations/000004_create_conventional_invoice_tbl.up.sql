CREATE TABLE IF NOT EXISTS "Conventional_Invoice" (
	"Conventional_Invoice_ID" integer GENERATED ALWAYS AS identity, --// PK
	"Name" text not null,
	"Ammount" integer not null,
	"Tenor" integer not null,
	"Grade" text not null,
	"Rate" integer not null,

    --// CONSTRAINT
    PRIMARY KEY("Conventional_Invoice_ID")
);
