CREATE TABLE IF NOT EXISTS "Financing" (
	"Financing_ID" integer GENERATED ALWAYS AS identity, --// PK
	"Name" text not null,
	"Count" integer not null,
	"Sub" text,

    --// CONSTRAINT
    PRIMARY KEY("Financing_ID")
);
