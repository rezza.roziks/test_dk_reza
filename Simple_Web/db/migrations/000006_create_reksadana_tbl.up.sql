CREATE TABLE IF NOT EXISTS "Reksadana" (
	"Reksadana_ID" integer GENERATED ALWAYS AS identity, --// PK
	"Name" text not null,
	"Ammount" integer not null,
	"Return" integer not null,

    --// CONSTRAINT
    PRIMARY KEY("Reksadana_ID")
);