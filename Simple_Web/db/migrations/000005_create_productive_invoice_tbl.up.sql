CREATE TABLE IF NOT EXISTS "Productive_Invoice" (
	"Productive_Invoice_ID" integer GENERATED ALWAYS AS identity, --// PK
	"Name" text not null,
	"Ammount" integer not null,
	"Grade" text not null,
	"Rate" integer not null,

    --// CONSTRAINT
    PRIMARY KEY("Productive_Invoice_ID")
);
