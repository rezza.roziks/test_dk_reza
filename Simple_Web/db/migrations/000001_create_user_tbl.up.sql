CREATE TABLE IF NOT EXISTS "User" (
	"User_ID" integer GENERATED ALWAYS AS identity, --// PK
	"Username" text not null,
	"Email" text not null,
	"Password" text not null,
    "Active_Access_Token" text,

    --// CONSTRAINT
    PRIMARY KEY("User_ID")
);
