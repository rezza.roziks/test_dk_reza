package db

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func Connect() *sqlx.DB {

	var (
		host     = os.Getenv("DB_Host")
		port, _  = strconv.Atoi(os.Getenv("DB_Port"))
		user     = os.Getenv("DB_User")
		password = os.Getenv("DB_Password")
		dbname   = os.Getenv("DB_Database")
	)

	var PsqlInfo = fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname,
	)

	pool, err := sqlx.Open("postgres", PsqlInfo)
	if err != nil {
		log.Fatalln("cannot open connection to db", err)
	}

	err = pool.Ping()
	if err != nil {
		log.Fatal("fail to create connection to db", err)
	}

	// Pool Settings
	pool.SetMaxOpenConns(25)                  // max open connection
	pool.SetMaxIdleConns(25)                  // max idle connection
	pool.SetConnMaxIdleTime(10 * time.Second) // max time connection may be idle
	pool.SetConnMaxLifetime(5 * time.Minute)  // max time connection may be reuse

	return pool
}
