package test

import (
	"simle_api/db"
	m_resp "simle_api/model"

	"github.com/gin-gonic/gin"
)

func TestRoute(router *gin.Engine) {
	v1 := router.Group("/test")

	// v1.Use(middleware.BearerDecoder())

	v1.GET("/get", func(c *gin.Context) {

		var (
			db_res   []TableName
			err      error
			response m_resp.Response
		)

		db := db.Connect()
		defer db.Close()

		db_res, err = getSomething(db)
		if err != nil {
			response = m_resp.ErrorInternalResponse(err.Error(), "function getSomething")
			c.JSON(response.Status_Code, response)
			return
		}

		response = m_resp.SuccessResponse(db_res)
		c.JSON(response.Status_Code, response)
	})
}
