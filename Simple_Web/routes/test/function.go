package test

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

func getSomething(db *sqlx.DB) ([]TableName, error) {

	result := []TableName{}

	// LIST TABLE
	query := (`
	
		select
			table_name 
		from information_schema.tables
		where table_schema = 'public' 

	`)
	rows, err := db.Queryx(query)
	if err != nil {
		return result, err
	}

	for rows.Next() {

		content := TableName{}
		err := rows.StructScan(&content)
		if err != nil {
			return result, err
		}
		result = append(result, content)
	}

	defer rows.Close()

	fmt.Println(result)

	return result, err
}
