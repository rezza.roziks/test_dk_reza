package main

import (
	"fmt"
	"math"
)

func findPrimeByRange(start int, end int) []int {

	// list result
	prime_l := []int{}

	// loop range
	for i := start; i <= end; i++ {

		// is <= 1
		if i <= 1 {
			continue
		}

		// is 2
		if i == 2 {
			prime_l = append(prime_l, i)
			continue
		}

		// check prime
		prime := true
		factor := int(math.Sqrt(float64(i)))
		for a := 2; a <= factor; a++ {
			if (i % a) == 0 {
				prime = false
				break
			}
		}

		// append
		if prime {
			prime_l = append(prime_l, i)
		}
	}

	fmt.Println("Result: \n", prime_l)
	return prime_l
}
