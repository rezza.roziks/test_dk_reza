package main

import (
	"fmt"
)

type CountResult struct {
	count int
	key   string
}

func countArr(ar []string) []CountResult {

	// var
	counted := []CountResult{}
	m := make(map[string]int)

	// count
	for _, v := range ar {

		if _, ok := m[v]; ok {
			counted[m[v]].count = counted[m[v]].count + 1
		} else {
			counted = append(
				counted,
				CountResult{
					key:   v,
					count: 1,
				},
			)
			m[v] = len(counted) - 1
		}

	}

	fmt.Println("Result: \n", counted)
	return counted
}
