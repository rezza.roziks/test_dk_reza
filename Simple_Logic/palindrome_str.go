package main

import (
	"fmt"
	"strings"
)

func isPallindrome(s string) bool {

	// ignore letter Case
	s = strings.ToLower(s)

	// var result
	is_palindrome := true

	// check
	for i, v := range s {

		if string(v) != string(s[len(s)-(1+i)]) {
			is_palindrome = false
			break
		}

	}

	fmt.Println("Result: \n", is_palindrome)
	return is_palindrome
}
