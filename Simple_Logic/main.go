package main

func main() {

	// 1. Check the string is palindrome or not
	// file palindrome_str.go
	isPallindrome("abcba")

	// 2. Find prime number by range
	// file prime_range.go
	findPrimeByRange(11, 40)

	// 3. Grouping array group into separate sub arraygroup
	// file array_group.go
	to_group := []string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}
	group(to_group)

	// 4. Count same element in an array withformat
	// file array_count.go
	to_count := []string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"}
	countArr(to_count)

}
