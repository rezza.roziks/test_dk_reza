package main

import (
	"encoding/json"
	"fmt"
)

func group(ar []string) [][]string {

	// var
	grouped := [][]string{}
	m := make(map[string]int)

	// group
	for _, v := range ar {

		if _, ok := m[v]; ok {
			grouped[m[v]] = append(grouped[m[v]], v)
		} else {
			grouped = append(grouped, []string{v})
			m[v] = len(grouped) - 1
		}

	}

	f_str, _ := json.Marshal(grouped)
	fmt.Println("Result: \n", string(f_str))
	return grouped
}
